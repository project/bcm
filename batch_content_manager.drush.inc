<?php

/**
 * @file
 * Unpublish all content Drush command
 */

/**
 * Implementation of hook_drush_command().
 */
function batch_content_manager_drush_command() {
  $items = array();
  $items['unpublish'] = array(
    'callback' => 'unpublish_node_drush_unpublish',
    'description' => 'Unpublish all nodes, nodes of a specific content type.',
    'options' => array(
      'roles' => 'pick roles',
    ),
    'examples' => array(
      'drush unpublish article' => 'unpublish all article nodes.',
      'drush unpublish all' => 'unpublish nodes of all types.',
     ),
    'aliases' => array('da'),
  );
  $items['block-user'] = array(
    'callback' => 'block_user_drush_unpublish',
    'description' => 'Block user, users of a specific role.',
    'options' => array(
      'roles' => 'pick roles',
    ),
    'examples' => array(
      'drush block-user users' => 'block all role user',
     ),
    'aliases' => array('da'),
  );

  $items['delete-all'] = array(
    'callback' => 'delete_all_drush_delete',
    'description' => 'Delete all nodes, nodes of a specific content type or users or taxonomy term.',
    'options' => array(
      'roles' => 'pick roles',
    ),
    'examples' => array(
      'drush delete-all article' => 'Delect all article nodes.',
      'drush delete-all all' => 'Delete nodes of all types.',
      'drush delete-all users' => 'Delete users.',
      'drush delete-all taxonomy' => 'Delete taxonomy term.',
     ),
    'aliases' => array('da'),
  );


  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function batch_content_manager_drush_help($section) {
  switch ($section) {
    case 'drush:unpublish':
    return dt("unpublish all nodes, nodes of a specific content type.");
    case 'drush:block-user':
    return dt("block all user, user of roles.");
    case 'drush:delete-all':
    return dt("Delete all nodes, nodes of a specific content type or users or taxonomy term.");
  }
}

/**
 * Drush callback to update content
 */
function unpublish_node_drush_unpublish() {
  $types = func_get_args();

  if ($types) {
     if (in_array('all', $types)) {
   
      drush_print('Unpublished ' . _unpublish_all_normal(TRUE, FALSE) . ' nodes');

    }else {
      drush_print('Unpublished ' . _unpublish_all_normal(FALSE, $types) . ' nodes');
    }
  }
  else {
    drush_print('Content types and number of nodes:');
    
    $result = db_query("SELECT type, COUNT(*) AS num FROM {node} GROUP BY type");
    $count = array();
    foreach ($result as $record) {
    $count[$record->type] = $record->num;
    }
    foreach (node_type_get_types() as $type => $info) {
    drush_print($info->type . ' ' . ($count[$type] + 0));
    }
  }
}

function block_user_drush_unpublish(){

  $type = func_get_args();

  if(in_array('users', $types)){

      if (!$types[1] && drush_get_option('roles')) {
        $choices = user_roles();
        $role = drush_choice($choices, dt("Choose a role to delete."));
        if ($role == 0) {
          return;
        }
      }
      else {
        $role = $types[1];
        if (strpos($role, ',')) {
          $role = explode(',', $role);
        }
      }

     drush_print('Unpublished ' . _block_all_user($role) . ' users');

  }



}


function delete_all_drush_delete() {
  $types = func_get_args();
  if ($types) {
    if (in_array('users', $types)) {
      if (!$types[1] && drush_get_option('roles')) {
        $choices = user_roles();
        $role = drush_choice($choices, dt("Choose a role to delete."));
        if ($role == 0) {
          return;
        }
      }
      else {
        $role = $types[1];
        if (strpos($role, ',')) {
          $role = explode(',', $role);
        }
      }
      drush_print('Deleted ' . delete_all_users_delete($role) . ' users');
    }
    elseif (in_array('all', $types)) {
      db_query("DELETE FROM {url_alias} WHERE source LIKE 'node/%%'");
      drush_print('Deleted ' . _delete_all_normal(TRUE, FALSE) . ' nodes');
      if (drush_get_option('reset')) {
        db_query("ALTER TABLE node AUTO_INCREMENT=1");
        db_query("ALTER TABLE node_revision AUTO_INCREMENT=1");
        if (module_exists('comment')) {
          db_query("ALTER TABLE comment AUTO_INCREMENT=1");
          drush_print('All node, revision and comment counts have been reset.');
        }
        else {
          drush_print('All node and revision counts have been reset.');
        }
      }
    }elseif(in_array('taxonomy', $types)){

      if (!$types[1] && drush_get_option('taxonomy')) {
        $vocabulary = taxonomy_vocabulary_get_names();
	$choices = array();
	foreach($vocabulary as  $voc){
	  $choices[$voc->vid] = $voc->name;
	}

        $taxonomy = drush_choice($choices, dt("Choose a taxonomy to delete."));
        if ($taxonomy == 0) {
          return;
        }
      }
      else {
        $taxonomy = $types[1];
        if (strpos($taxonomy, ',')) {
          $taxonomy = explode(',', $taxonomy);
        }
      }
      drush_print('Deleted ' . delete_all_term_delete($taxonomy) . ' terms');
	
    }
    else {
      drush_print('Deleted ' . _delete_all_normal(FALSE, $types) . ' nodes');
    }
  }
  else {
    drush_print('Content types and number of nodes:');
    // This should be refactored back into delete_all_content
    $result = db_query("SELECT type, COUNT(*) AS num FROM {node} GROUP BY type");
    $count = array();
    foreach ($result as $record) {
    $count[$record->type] = $record->num;
    }
    foreach (node_type_get_types() as $type => $info) {
    drush_print($info->type . ' ' . ($count[$type] + 0));
    }
  }
}

function delete_all_term_delete($taxonomy){

 $results = array();
 $deleted = 0;

  if (is_array($taxonomy)) {
    foreach($taxonomy as $name){
    $vid = db_query('SELECT vid FROM {taxonomy_vocabulary} WHERE name=:name ',array(':name'=>$name))->fetchfield();
      $results[] = db_query(
        'SELECT tid FROM {taxonomy_term_data} WHERE vid = :vid',
        array(':vid' => $vid)
      );
    }

  }
  foreach ($results as $result) {
    if ($result) {
      foreach ($result as $data) {
   	 taxonomy_term_delete($data->tid);
	 $deleted++;
      }
    }
  }

 return $deleted;

}

function _block_all_user($role){

 $results = array();
 $blocked = 0;
  if (is_array($role) && count($role) > 0) {
    foreach($role as $name){
      $rid = db_query('SELECT rid FROM  {role} WHERE name =:name ',array(':name'=>$name))->fetchfield();
      $results[] = db_query(
        'SELECT uid FROM {users_roles} WHERE rid = :rid',
        array(':rid' => $rid)
      );
    }
  }
  foreach ($results as $result) {
    if ($result) {
      foreach ($result as $data) {
         $user = user_load($data->uid);
	 $user->status = 0;
	 user_save($user);
	 $blocked++;
      }
    }
  }

 return $blocked;
}


function _unpublish_all_normal($all, $types){

  $results = array();
  $unpublished = 0;
  if (is_array($types) && count($types) > 0) {
    foreach ($types as $type) {
      $results[] = db_query(
        'SELECT nid FROM {node} WHERE type = :type',
        array(':type' => $type)
      );
    }
  }
  else {
    $results[] = db_query(
      'SELECT nid FROM {node}'
    );
  }
  foreach ($results as $result) {
    if ($result) {
      foreach ($result as $data) {
        set_time_limit(30);
        $node = node_load($data->nid);
	$node->status = 0;
	node_save($node);
        $unpublished ++;
      }
    }
  }
  return $unpublished;

}


function _delete_all_normal($all, $types) {
  $results = array();
  $deleted = 0;
  if (is_array($types) && count($types) > 0) {
    foreach ($types as $type) {
      $results[] = db_query(
        'SELECT nid FROM {node} WHERE type = :type',
        array(':type' => $type)
      );
    }
  }
  else {
    $results[] = db_query(
      'SELECT nid FROM {node}'
    );
  }
  foreach ($results as $result) {
    if ($result) {
      foreach ($result as $data) {
        set_time_limit(30);
        node_delete($data->nid);
        $deleted ++;
      }
    }
  }
  return $deleted;
}

function delete_all_users_delete($roles = NULL) {
  $count = 0;
  if (!$roles) {
    $result = db_query('SELECT uid FROM {users} WHERE uid > 1');
    foreach ($result as $data) {
      user_delete($data->uid);
      $count++;
    }
    // Delete the URL aliases
    db_query("DELETE FROM {url_alias} WHERE source LIKE 'user/%%'");
  }
  else {
    if (is_array($roles)) {
      $result = array();
      foreach ($roles as $role) {
        $rid = db_select('role', 'r')->fields('r', array('rid'))->condition('name', $role, '=')->execute()->fetchField();
        $result = array_merge($result, db_select('users_roles', 'ur')->fields('ur', array('uid'))->condition('rid', $rid, '=')->execute()->fetchCol('uid'));
      }
    }
    else {
      $rid = db_select('role', 'r')->fields('r', array('rid'))->condition('name', $roles, '=')->execute()->fetchField();
      $result = db_select('users_roles', 'ur')->fields('ur', array('uid'))->condition('rid', $rid, '=')->execute()->fetchCol('uid');
    }
    foreach ($result as $data) {
      user_delete($data);
      $count++;
    }
    // @TODO Delete individual aliases
  }
  return $count;
}
