if (Drupal.jsEnabled) {
  $(document).ready(function () {
    function check_buttons() {
      if ($('#unpublish-all-content .unpublish-all').is(':checked')) {
        $('#unpublish-all-content .form-checkboxes input').attr('checked', 'checked').attr('disabled', 'disabled');
      }
      else {
        $('#unpublish-all-content .form-checkboxes input').removeAttr('checked').removeAttr('disabled');
        if ($('#unpublish-all-content fieldset').is('.collapsed')) {
          Drupal.toggleFieldset($('#unpublish-all-content fieldset'));
        }
      }
    }

    $('#unpublish-all-content .unpublish-all').click(function () {
      check_buttons();
    });

    check_buttons();
  }); 
}
